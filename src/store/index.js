import Vue from 'vue'
import Vuex from 'vuex'

import app from '../main'
import menu from './modules/menu'
import user from './modules/user'
import chat from './modules/chat'
import todo from './modules/todo'
import survey from './modules/survey'
import simulators from "./modules/simulators";
import lessons from "./modules/lessons"
import pages from "./modules/pages";
import pageBlocks from "./modules/pageBlocks";
import promocodes from "./modules/promocodes";
import simulatorGroup from "./modules/simulatorGroup";
import uploadImage from "@/store/modules/uploadImage";
import letters from "@/store/modules/letters";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
    changeLang (state, payload) {
      app.$i18n.locale = payload
      localStorage.setItem('currentLanguage', payload)
    }
  },
  actions: {
    setLang ({ commit }, payload) {
      commit('changeLang', payload)
    }
  },
  modules: {
    menu,
    user,
    chat,
    todo,
    survey,
    simulators,
    lessons,
    pages,
    pageBlocks,
    promocodes,
    simulatorGroup,
    uploadImage,
    letters
  }
})
