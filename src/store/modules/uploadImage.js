import API from 'Api'

const state = {
  func_upload_image: function(blobInfo, success, failure, progress) {
    console.log(blobInfo)
    var xhr, formData;

    xhr = new XMLHttpRequest();
    xhr.withCredentials = false;
    xhr.open('POST', 'https://api.mysimulator.ru/admin_pannel/upload_image/');

    xhr.upload.onprogress = function (e) {
      progress(e.loaded / e.total * 100);
    };

    xhr.onload = function() {
      var json;

      if (xhr.status === 403) {
        failure('HTTP Error: ' + xhr.status, { remove: true });
        return;
      }

      if (xhr.status < 200 || xhr.status >= 300) {
        failure('HTTP Error: ' + xhr.status);
        return;
      }

      json = JSON.parse(xhr.responseText);

      if (!json || typeof json.location != 'string') {
        failure('Invalid JSON: ' + xhr.responseText);
        return;
      }

      success('https://api.mysimulator.ru' + json.location);
    };

    xhr.onerror = function () {
      failure('Image upload failed due to a XHR Transport error. Code: ' + xhr.status);
    };

    formData = new FormData();
    formData.append('image', blobInfo.blob(), blobInfo.filename());

    xhr.send(formData);
  },
  kek: 'lol'
}

const getters = {
  upload_image: state => {
    return state.func_upload_image
  }
}

const mutations = {

}

const actions = {
  // upload_image(blobInfo, success, failure, progress) {
  //   var xhr, formData;
  //
  //   xhr = new XMLHttpRequest();
  //   xhr.withCredentials = false;
  //   xhr.open('POST', 'https://api.mysimulator.ru/admin_pannel/upload_image/');
  //
  //   xhr.upload.onprogress = function (e) {
  //     progress(e.loaded / e.total * 100);
  //   };
  //
  //   xhr.onload = function() {
  //     var json;
  //
  //     if (xhr.status === 403) {
  //       failure('HTTP Error: ' + xhr.status, { remove: true });
  //       return;
  //     }
  //
  //     if (xhr.status < 200 || xhr.status >= 300) {
  //       failure('HTTP Error: ' + xhr.status);
  //       return;
  //     }
  //
  //     json = JSON.parse(xhr.responseText);
  //
  //     if (!json || typeof json.location != 'string') {
  //       failure('Invalid JSON: ' + xhr.responseText);
  //       return;
  //     }
  //
  //     success('https://api.mysimulator.ru' + json.location);
  //   };
  //
  //   xhr.onerror = function () {
  //     failure('Image upload failed due to a XHR Transport error. Code: ' + xhr.status);
  //   };
  //
  //   formData = new FormData();
  //   formData.append('image', blobInfo.blob(), blobInfo.filename());
  //
  //   xhr.send(formData);
  // },
}



export default {
  state,
  getters,
  mutations,
  actions
}
