import API from 'Api'
import router from "../../router";

const state = {
  simulatorsList: [],
  currSim: null
}

const getters = {
  getSimulatorsList: state => {
    return state.simulatorsList;
  },
  getCurrSim: state => {
    return state.currSim;
  }
}

const mutations = {
  setAllSimulators(state, payload){
    state.simulatorsList = payload;
  },
  setCurrSim(state, payload){
    state.currSim = payload;
  }
}

const actions = {
  getAllSimulators({dispatch, commit}){
    const config = {
      headers: { Authorization: `Bearer ${localStorage.getItem('user')}`}
    };
    API
      .get("admin_pannel/simulators/", config)
      .then(response => {
        commit('setAllSimulators', response.data);
      })
      .catch(function (error) {
        console.log(error)
        if(error.response.status == 404)
        {

        }
        if(error.response.status == 401)
        {
          dispatch('signOut');
          router.push('/user');
        }
        if(error.response.status == 500)
        {
          this.$bvToast.toast('Ошибка сервера. Невозможно выполнить запрос.', {
            title: 'Уведомление',
            variant: 'danger',
            solid: true
          })
        }

      }.bind(this))
  }
}



export default {
  state,
  getters,
  mutations,
  actions
}
