import firebase from 'firebase/app'
import 'firebase/auth'
import API from "@/api";


export default {
  state: {
    currentUser: (localStorage.getItem('user') != null ? localStorage.getItem('user') : null),
    loginError: null,
    processing: false,
    forgotMailSuccess:null,
    resetPasswordSuccess:null
  },
  getters: {
    currentUser: state => state.currentUser,
    processing: state => state.processing,
    loginError: state => state.loginError,
    forgotMailSuccess: state => state.forgotMailSuccess,
    resetPasswordSuccess:state => state.resetPasswordSuccess,
  },
  mutations: {
    setUser(state, payload) {
      state.currentUser = payload
      state.processing = false
      state.loginError = null
    },
    setLogout(state) {
      state.currentUser = null
      state.processing = false
      state.loginError = null
    },
    setProcessing(state, payload) {
      state.processing = payload
      state.loginError = null
    },
    setError(state, payload) {
      state.loginError = payload
      state.currentUser = null
      state.processing = false
    },
    setForgotMailSuccess(state) {
      state.loginError = null
      state.currentUser = null
      state.processing = false
      state.forgotMailSuccess=true
    },
    setResetPasswordSuccess(state) {
      state.loginError = null
      state.currentUser = null
      state.processing = false
      state.resetPasswordSuccess=true
    },
    clearError(state) {
      state.loginError = null
    }
  },
  actions: {
    login({ commit }, payload) {
      commit('clearError')
      commit('setProcessing', true)
      const data = new FormData();
      console.log(payload)
      if (!payload.email || !payload.password){
        this.$bvToast.toast('Заполните все поля', {
          title: 'Уведомление',
          variant: 'info',
          solid: true
        });
        return false
      }
      data.append("email", payload.email);
      data.append("password", payload.password);
      data.append("username", payload.email);
      API.post("token/", data).then(response => {
        console.log(response.data.token)
        const item = response.data.token
        localStorage.setItem('user', item)
        commit('setUser', response.data.token)
      })
      .catch(function (error) {
        console.log(error)
        if(error.response.status == 401)
        {
          localStorage.removeItem('user')
            commit('setError', 'Введен неправильный пароль')
            setTimeout(() => {
              commit('clearError')
            }, 3000)

        }

      }.bind(this));
    },
    forgotPassword({ commit }, payload) {
      if (!payload.email){
        this.$bvToast.toast('Введите почту', {
          title: 'Уведомление',
          variant: 'info',
          solid: true
        });
        return false
      }
      commit('clearError')
      commit('setProcessing', true)
      firebase
        .auth()
        .sendPasswordResetEmail(payload.email)
        .then(
          user => {
            commit('clearError')
            commit('setForgotMailSuccess')
          },
          err => {
            commit('setError', err.message)
            setTimeout(() => {
              commit('clearError')
            }, 3000)
          }
        )
    },
    resetPassword({ commit }, payload) {
      if (!payload.email || !payload.password){
        this.$bvToast.toast('Введите новый пароль', {
          title: 'Уведомление',
          variant: 'info',
          solid: true
        });
        return false
      }
      commit('clearError')
      commit('setProcessing', true)
      firebase
        .auth()
        .confirmPasswordReset(payload.resetPasswordCode,payload.newPassword)
        .then(
          user => {
            commit('clearError')
            commit('setResetPasswordSuccess')
          },
          err => {
            commit('setError', err.message)
            setTimeout(() => {
              commit('clearError')
            }, 3000)
          }
        )
    },



    /*
       return await auth.(resetPasswordCode, newPassword)
        .then(user => user)
        .catch(error => error);
    */
    signOut({ commit }) {
      localStorage.removeItem('user');
      commit('setLogout')

    }
  }
}
