import API from 'Api'
import router from "../../router";

const state = {
    pagesList: null
}

const getters = {
    getPagesList: state => {
        return state.pagesList;
    }
}

const mutations = {
    setAllPages(state, payload){
        state.pagesList = payload;
    }
}

const actions = {
    getAllPages({dispatch, commit}, id){
        const config = {
            headers: { Authorization: `Bearer ${localStorage.getItem('user')}`}
        };
        API
            .get('admin_pannel/lesson/' + id + '/pages/', config)
            .then(response => {
                commit('setAllPages', response.data);
            })
            .catch(function (error) {
              if(error.response.status == 404)
              {

              }
              if(error.response.status == 401)
              {
                dispatch('signOut');
                router.push('/user');

              }
              if(error.response.status == 500)
              {
                this.$bvToast.toast('Ошибка сервера. Невозможно выполнить запрос.', {
                  title: 'Уведомление',
                  variant: 'danger',
                  solid: true
                })
              }
              if(error.response.status == 403)
              {
                console.log('Get pages: you do not have permission to perform this action')
              }
            }.bind(this));
    }
}

export default {
    state,
    getters,
    mutations,
    actions
}
