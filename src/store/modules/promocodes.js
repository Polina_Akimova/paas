import API from 'Api'
import router from "../../router";

const state = {
  promoList: []
}

const getters = {
  getPromoList: state => {
    return state.promoList;
  }
}

const mutations = {
  setAllPromo(state, payload){
    state.promoList = payload;
  }
}

const actions = {
  getAllPromo({dispatch, commit}, id){
    const config = {
      headers: { Authorization: `Bearer ${localStorage.getItem('user')}`}
    };
    API
      .get('admin_pannel/promocodes/?simulator=' + id, config)
      .then(response => {
        commit('setAllPromo', response.data);
      })
      .catch(function (error) {
        if(error.response.status == 404)
        {

        }
        if(error.response.status == 401)
        {
          dispatch('signOut');
          router.push('/user');

        }
        if(error.response.status == 500)
        {
          this.$bvToast.toast('Ошибка сервера. Невозможно выполнить запрос.', {
            title: 'Уведомление',
            variant: 'danger',
            solid: true
          })
        }
        if(error.response.status == 400)
        {
          this.$bvToast.toast('Симулятор не принадлежит пользователю', {
            title: 'Уведомление',
            variant: 'info',
            solid: true
          })
        }
      }.bind(this));
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
