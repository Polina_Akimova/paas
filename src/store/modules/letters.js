import API from 'Api'
import router from "../../router";

const state = {
  lettersList: null,
  lettersGroupList: null
}

const getters = {
  getLettersList: state => {
    return state.lettersList;
  },
  getLettersGroupList: state => {
    return state.lettersGroupList;
  }
}

const mutations = {
  setAllLetters(state, payload){
    state.lettersList = payload;
  },
  setAllLettersGroup(state, payload){
    state.lettersGroupList = payload;
  }
}

const actions = {
  getAllLetters({dispatch, commit}, id){
    const config = {
      headers: { Authorization: `Bearer ${localStorage.getItem('user')}`}
    };
    API
      .get('admin_pannel/emails/?search=simulator&simulator=' + id, config)
      .then(response => {
        commit('setAllLetters', response.data);
      })
      .catch(function (error) {
        if(error.response.status == 404)
        {

        }
        if(error.response.status == 401)
        {
          dispatch('signOut');
          router.push('/user');

        }
        if(error.response.status == 500)
        {
          this.$bvToast.toast('Ошибка сервера. Невозможно выполнить запрос.', {
            title: 'Уведомление',
            variant: 'danger',
            solid: true
          })
        }
        if(error.response.status == 400)
        {
          this.$bvToast.toast('Симулятор не принадлежит пользователю', {
            title: 'Уведомление',
            variant: 'info',
            solid: true
          })
        }
      }.bind(this));
  },
  getLettersByGroup({dispatch, commit}, groupId){
    const config = {
      headers: { Authorization: `Bearer ${localStorage.getItem('user')}`}
    };
    API
      .get('admin_pannel/emails/?search=group&group=' + groupId, config)
      .then(response => {
        commit('setAllLettersGroup', response.data);
      })
      .catch(function (error) {
        if(error.response.status == 404)
        {

        }
        if(error.response.status == 401)
        {
          dispatch('signOut');
          router.push('/user');

        }
        if(error.response.status == 500)
        {
          this.$bvToast.toast('Ошибка сервера. Невозможно выполнить запрос.', {
            title: 'Уведомление',
            variant: 'danger',
            solid: true
          })
        }
        if(error.response.status == 400)
        {
          this.$bvToast.toast('Симулятор не принадлежит пользователю', {
            title: 'Уведомление',
            variant: 'info',
            solid: true
          })
        }
      }.bind(this));
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
