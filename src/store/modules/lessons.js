import API from 'Api'
import router from "../../router";

const state = {
  lessonsList: null
}

const getters = {
  getLessonsList: state => {
    return state.lessonsList
  }
}

const mutations = {
  setAllLessons(state, payload){
    state.lessonsList = payload;
  }
}

const actions = {
  getAllLessons({dispatch, commit}, simulatorId){
    API.get('admin_pannel/simulator/' + simulatorId + '/lessons/',
      {headers: {Authorization: `Bearer ${localStorage.getItem('user')}`}})
      .then(response => {
        commit('setAllLessons', response.data)
      })
      .catch(function (error) {
        if(error.response.status == 404)
        {

        }
        if(error.response.status == 401)
        {
          dispatch('signOut');
          router.push('/user');

        }
        if(error.response.status == 500)
        {
          this.$bvToast.toast('Ошибка сервера. Невозможно выполнить запрос.', {
            title: 'Уведомление',
            variant: 'danger',
            solid: true
          })
        }
        if(error.response.status == 403)
        {
          console.log('Get lessons: you do not have permission to perform this action')
        }

      }.bind(this))
  }
}



export default {
  state,
  getters,
  mutations,
  actions
}
