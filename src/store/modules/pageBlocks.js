import API from 'Api'
import router from '../../router.js'


export default {
  state: {
    blockList: null,
    persons: [],
    chapters: [],
    messages: [],
    theory: [],
    addType: ''
  },
  getters: {
    getAddType: state => {
      return state.addType;
    },
    getBlockList: state => {
      return state.blockList;
    },
    getPersons: state => {
      return state.persons;
    },
    getChapters: state => {
      return state.chapters;
    },
    getMessages: state => {
      return state.messages
    },
    getTheory: state => {
      return state.theory
    }
  },
  mutations: {
    setAllBlocks(state, payload){
      state.blockList = payload;
    },
    setAllPersons(state, payload){
      state.persons = payload;
    },
    setAllChapters(state, payload){
      state.chapters = payload;
    },
    setAllMessages(state, payload){
      state.messages = payload;
    },
    setAllTheory(state, payload){
      state.theory = payload;
    },
    setNewAddType(state, payload){
      console.log(payload)
      state.addType = payload;
    }
  },
  actions: {
    getNewAddType({commit}, type){
      commit('setNewAddType', type);
    },
    getAllBlocks({dispatch, commit}, id){
      const config = {
        headers: { Authorization: `Bearer ${localStorage.getItem('user')}`}
      };
      API
        .get('admin_pannel/page/' + id + '/', config)
        .then(response => {
          console.log(response.data)
          commit('setAllBlocks', response.data);
        })
        .catch(function (error) {
          if(error.response.status == 404)
          {

          }
          if(error.response.status == 401)
          {
            dispatch('signOut');
            router.push('/user');

          }
          if(error.response.status == 500)
          {
            this.$bvToast.toast('Ошибка сервера. Невозможно выполнить запрос.', {
              title: 'Уведомление',
              variant: 'danger',
              solid: true
            })
          }
          if(error.response.status == 400)
          {
            this.$bvToast.toast('Симулятор не принадлежит пользователю', {
              title: 'Уведомление',
              variant: 'info',
              solid: true
            })
          }
        }.bind(this));
    },
    getAllPersons({dispatch, commit}, id){
      const config = {
        headers: { Authorization: `Bearer ${localStorage.getItem('user')}`}
      };
      API
        .get('admin_pannel/characters/?simulator=' + id, config)
        .then(response => {
          commit('setAllPersons', response.data)
        })
        .catch(function (error) {
          console.log(error)
          if(error.response.status == 404)
          {

          }
          if(error.response.status == 400)
          {
            this.$bvToast.toast('Симулятор не принадлежит пользователю', {
              title: 'Уведомление',
              variant: 'info',
              solid: true
            })
          }
          if(error.response.status == 500)
          {
            this.$bvToast.toast('Ошибка сервера. Невозможно выполнить запрос.', {
              title: 'Уведомление',
              variant: 'danger',
              solid: true
            })
          }
          if(error.response.status == 401)
          {
            dispatch('signOut');
            router.push('/user');
            //ошибка токена. Пытается ломануть нас. Выкинуть на главную

          }
        }.bind(this))
    },
    getAllMessages({dispatch, commit}, id){
      const config = {
        headers: { Authorization: `Bearer ${localStorage.getItem('user')}`}
      };
      API
        .get('admin_pannel/page/' + id + '/?type=message', config)
        .then(response => {
          commit('setAllMessages', response.data)
        })
        .catch(function (error) {
          console.log(error)
          if(error.response.status == 404)
          {

          }
          if(error.response.status == 400)
          {
            this.$bvToast.toast('Симулятор не принадлежит пользователю', {
              title: 'Уведомление',
              variant: 'info',
              solid: true
            })
          }
          if(error.response.status == 500)
          {
            this.$bvToast.toast('Ошибка сервера. Невозможно выполнить запрос.', {
              title: 'Уведомление',
              variant: 'danger',
              solid: true
            })
          }
          if(error.response.status == 401)
          {
            dispatch('signOut');
            router.push('/user');
            //ошибка токена. Пытается ломануть нас. Выкинуть на главную

          }
        }.bind(this))
    },
    getAllChapters({dispatch, commit}, id){
      const config = {
        headers: { Authorization: `Bearer ${localStorage.getItem('user')}`}
      };
      API
        .get('admin_pannel/theory_chapters/?simulator=' + id, config)
        .then(response => {
          commit('setAllChapters', response.data)
        })
        .catch(function (error) {
          console.log(error)
          if(error.response.status == 404)
          {

          }
          if(error.response.status == 400)
          {
            this.$bvToast.toast('Симулятор не принадлежит пользователю', {
              title: 'Уведомление',
              variant: 'info',
              solid: true
            })
          }
          if(error.response.status == 500)
          {
            this.$bvToast.toast('Ошибка сервера. Невозможно выполнить запрос.', {
              title: 'Уведомление',
              variant: 'danger',
              solid: true
            })
          }
          if(error.response.status == 401)
          {
            dispatch('signOut');
            router.push('/user');
            //ошибка токена. Пытается ломануть нас. Выкинуть на главную

          }
        }.bind(this))
    },
    getAllTheory({dispatch, commit}, id){
      const config = {
        headers: { Authorization: `Bearer ${localStorage.getItem('user')}`}
      };
      API
        .get('admin_pannel/theories/?simulator=' + id, config)
        .then(response => {
          console.log(response);
          commit('setAllTheory', response.data)
        })
        .catch(function (error) {
          console.log(error)
          if(error.response.status == 404)
          {

          }
          if(error.response.status == 400)
          {
            this.$bvToast.toast('Симулятор не принадлежит пользователю', {
              title: 'Уведомление',
              variant: 'info',
              solid: true
            })
          }
          if(error.response.status == 500)
          {
            this.$bvToast.toast('Ошибка сервера. Невозможно выполнить запрос.', {
              title: 'Уведомление',
              variant: 'danger',
              solid: true
            })
          }
          if(error.response.status == 401)
          {
            dispatch('signOut');
            router.push('/user');
            //ошибка токена. Пытается ломануть нас. Выкинуть на главную

          }
        }.bind(this))
    }
  }

}
