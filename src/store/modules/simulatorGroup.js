import API from 'Api'
import router from "../../router";

const state = {
  simGroupsList: [],
  simGroup: []
}

const getters = {
  getSimGroupsList: state => {
    return state.simGroupsList;
  },
  getSimGroup: state => {
    return state.simGroup;
  }
}

const mutations = {
  setAllSimGroups(state, payload){
    state.simGroupsList = payload;
  },
  setSimGroup(state, payload){
    state.simGroup = payload;
  }
}

const actions = {
  getAllSimGroups({dispatch, commit}){
    const config = {
      headers: { Authorization: `Bearer ${localStorage.getItem('user')}`}
    };
    API
      .get('admin_pannel/groups/', config)
      .then(response => {
        commit('setAllSimGroups', response.data);
      })
      .catch(function (error) {
        if(error.response.status == 404)
        {

        }
        if(error.response.status == 500)
        {
          this.$bvToast.toast('Ошибка сервера. Невозможно выполнить запрос.', {
            title: 'Уведомление',
            variant: 'danger',
            solid: true
          })
        }
        if(error.response.status == 401)
        {
          dispatch('signOut');
          router.push('/user');

        }
      }.bind(this));
  },
  getCurrSimGroup({dispatch, commit}, id){
    const config = {
      headers: { Authorization: `Bearer ${localStorage.getItem('user')}`}
    };
    API
      .get('admin_pannel/group/' + id, config)
      .then(response => {
        commit('setSimGroup', response.data);
      })
      .catch(function (error) {
        if(error.response.status == 404)
        {

        }
        if(error.response.status == 500)
        {
          this.$bvToast.toast('Ошибка сервера. Невозможно выполнить запрос.', {
            title: 'Уведомление',
            variant: 'danger',
            solid: true
          })
        }
        if(error.response.status == 401)
        {
          dispatch('signOut');
          router.push('/user');

        }
      }.bind(this));
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
